registerSettingsPage(({ settings }) => (
  <Page>
    <Section
      title={
        <Text bold align="center">
          Lt Cmd Settings
        </Text>
      }
    >
        <Text>Watch Background Color</Text>
        <ColorSelect
            settingsKey="color_background"
            colors={[
            {color: 'tomato'},
            {color: 'sandybrown'},
            {color: 'gold'},
            {color: 'aquamarine'},
            {color: 'deepskyblue'},
            {color: 'plum'},
            {color: 'black'},
            {color: 'white'}


        ]}
        />

        <Text>Watch Text Color</Text>
        <ColorSelect
            settingsKey="color_text"
            colors={[
            {color: 'tomato'},
            {color: 'sandybrown'},
            {color: 'gold'},
            {color: 'aquamarine'},
            {color: 'deepskyblue'},
            {color: 'plum'},
            {color: 'black'},
            {color: 'white'}
        ]}
        />

    </Section>
  </Page>
));
