import { settingsStorage } from "settings";
import { me as companion } from "companion";
import { sendValue } from "./utils";

const COLOR_BACKGROUND = "color_background";
const COLOR_TEXT = "color_text";


// Settings have been changed
settingsStorage.addEventListener("change", (evt) => {
    sendValue(evt.key, evt.newValue);
});

// Settings were changed while the companion was not running
if (companion.launchReasons.settingsChanged) {
    // Send the value of the setting
    send_settings();
}

if (companion.launchReasons.wokenUp) {
    run_after_wake();
}

function run_after_wake() {
    
}

function send_settings() {
    sendValue(COLOR_BACKGROUND, settingsStorage.getItem(COLOR_BACKGROUND));
    sendValue(COLOR_TEXT, settingsStorage.getItem(COLOR_TEXT));
}