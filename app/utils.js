export function zero_pad(time_unit) {
    return (parseInt(time_unit) < 10) ? `0${time_unit}` : time_unit;
}