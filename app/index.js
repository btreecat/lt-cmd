// Imports
import clock from "clock";
import * as document from "document";
import { HeartRateSensor } from "heart-rate";
import { display } from "display";
import { BodyPresenceSensor } from "body-presence";
import { today } from "user-activity";
import * as utils from "./utils";
import * as messaging from "messaging";


// Config
clock.granularity = "minutes"; // seconds, minutes, hours

// Reserved Values
const COLOR_BACKGROUND = "color_background";
const COLOR_TEXT = "color_text";

// Sensors
const hrm_sensor = new HeartRateSensor();
const body_presence = new BodyPresenceSensor();


// Watchface Elements
const clock_label = document.getElementById("clock_label");
const date_label = document.getElementById("date_label");
const day_label = document.getElementById("day_label");
const hrm_label = document.getElementById("hrm_label");
const floors_label = document.getElementById("floors_label");
const distance_label = document.getElementById("distance_label");
const steps_label = document.getElementById("steps_label");
const calories_label = document.getElementById("calories_label");
const background = document.getElementById("background");
const text_elements = document.getElementsByTagName("text")


clock.addEventListener("tick", (evt) => {
    const hour = utils.zero_pad(evt.date.getHours());
    const minutes = utils.zero_pad(evt.date.getMinutes());
    clock_label.text =  `${hour}:${minutes}`;
    date_label.text = `${evt.date.getFullYear()}-${evt.date.getMonth()+1}-${evt.date.getDate()}`;
    day_label.text = evt.date.toString().slice(0, 3);

    if (today.local.elevationGain !== undefined) {
        floors_label.text = today.adjusted.elevationGain;
    }
    calories_label.text = today.adjusted.calories
    steps_label.text = today.adjusted.steps
    distance_label.text = today.adjusted.distance

});

// We should check if the device supports the feature
if (HeartRateSensor) {
    // Event for HR readings
    hrm_sensor.addEventListener("reading", () => {
        hrm_label.text = hrm_sensor.heartRate
    });
    // Event for Screen On
    display.addEventListener("change", () => {
        // Automatically stop the sensor when the screen is off to conserve battery
    display.on ? hrm_sensor.start() : hrm_sensor.stop();
    });
    // Ensure that the device supports this feature
    if (BodyPresenceSensor) {
        // Event for body presence
        body_presence.addEventListener("reading", () => {
            // Automatically stop the sensor when the watch is not being worn to conserve battery
            body_presence.present ? body_presence.start() : body_presence.stop();
        });
        body_presence.start();
    }
    hrm_sensor.start();
}


messaging.peerSocket.addEventListener("message", (evt) => {
    if (evt && evt.data && evt.data.key === COLOR_BACKGROUND) {
        background.style.fill = evt.data.value;
    }
    if (evt && evt.data && evt.data.key === COLOR_TEXT) {
        text_elements.forEach(element => element.style.fill = evt.data.value);
    }
});





